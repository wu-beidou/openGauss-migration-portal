<?xml version="1.0" encoding="UTF-8" ?>
<!--
  ~ Copyright (c) 2022-2022 Huawei Technologies Co.,Ltd.
  ~
  ~ openGauss is licensed under Mulan PSL v2.
  ~ You can use this software according to the terms and conditions of the Mulan PSL v2.
  ~ You may obtain a copy of Mulan PSL v2 at:
  ~
  ~           http://license.coscl.org.cn/MulanPSL2
  ~
  ~ THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
  ~ EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
  ~ MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
  ~ See the Mulan PSL v2 for more details.
  -->

<Configuration status="INFO" monitorInterval="600">
    <Properties>
        <!--Property LOG_HOME must be absolute path dir -->
        <Property name="LOG_HOME">logs</Property>
        <Property name="LOG_LEVEL">INFO</Property>
        <Property name="LOG_NAME">${sys:logName}</Property>
        <Property name="LOG_DEBUGGER">debugger</Property>
        <Property name="LOG_KAFKA">kafka</Property>
        <Property name="LOG_BUSINESS">business</Property>
        <property name="LOG_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level [%c{1.}] - %msg%n"/>
        <property name="LOG_BUSINESS_PATTERN" value="%d{yyyy-MM-dd HH:mm:ss.SSS}[%thread] - %msg%n"/>
    </Properties>
    <Appenders>
        <Console name="console_out_appender" target="SYSTEM_OUT">
            <Filters>
                <ThresholdFilter level="${LOG_LEVEL}" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
            <PatternLayout pattern="${LOG_PATTERN}"/>
        </Console>

        <RollingFile name="progress" immediateFlush="true" fileName="${LOG_HOME}/${LOG_NAME}.log"
                     filePattern="${LOG_HOME}/history/${LOG_NAME} - %d{yyyy-MM-dd}.log.gz">
            <PatternLayout pattern="${LOG_PATTERN}"/>
            <Policies>
                <SizeBasedTriggeringPolicy size="20MB"/>
            </Policies>
            <Filters>
                <ThresholdFilter level="info" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
        </RollingFile>
        <RollingFile name="business" immediateFlush="true" fileName="${LOG_HOME}/${LOG_BUSINESS}-${LOG_NAME}.log"
                     filePattern="${LOG_HOME}/history/${LOG_BUSINESS}-${LOG_NAME} - %d{yyyy-MM-dd}.log.gz">
            <PatternLayout pattern="${LOG_BUSINESS_PATTERN}"/>
            <Policies>
                <SizeBasedTriggeringPolicy size="20MB"/>
            </Policies>
            <Filters>
                <ThresholdFilter level="info" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
        </RollingFile>
        <RollingFile name="kafka" immediateFlush="true" fileName="${LOG_HOME}/${LOG_KAFKA}-${LOG_NAME}.log"
                     filePattern="${LOG_HOME}/history/${LOG_KAFKA}-${LOG_NAME} - %d{yyyy-MM-dd}.log.gz">
            <PatternLayout pattern="${LOG_BUSINESS_PATTERN}"/>
            <Policies>
                <SizeBasedTriggeringPolicy size="20MB"/>
            </Policies>
            <Filters>
                <ThresholdFilter level="info" onMatch="ACCEPT" onMismatch="DENY"/>
            </Filters>
        </RollingFile>
        <!--
             <RollingFile name="debugger" immediateFlush="true" fileName="${LOG_HOME}/${LOG_DEBUGGER}-${LOG_NAME}.log"
                          filePattern="${LOG_HOME}/history/${LOG_NAME} - %d{yyyy-MM-dd}.log.gz">
                 <PatternLayout pattern="${LOG_PATTERN}"/>
                 <Policies>
                     <SizeBasedTriggeringPolicy size="20MB"/>
                 </Policies>
             </RollingFile>
                -->
         </Appenders>

         <Loggers>
             <root level="${LOG_LEVEL}">
                 <appender-ref ref="console_out_appender"/>
             </root>
             <logger name="progress" level="${LOG_LEVEL}">
                 <appender-ref ref="progress"/>
             </logger>
             <logger name="business" level="${LOG_LEVEL}">
                 <appender-ref ref="business"/>
             </logger>
             <logger name="kafka" level="${LOG_LEVEL}">
                 <appender-ref ref="kafka"/>
             </logger>
             <!--
               <logger name="debugger" level="${LOG_LEVEL}">
                    <appender-ref ref="debugger"/>
                </logger>
              -->
               <logger name="org.apache.kafka" level="ERROR"/>
               <logger name="org.opengauss.datachecker.check.service.EndpointManagerService" level="ERROR"/>
               <logger name="org.springframework" level="INFO"/>
               <logger name="_org.springframework.web" level="ERROR"/>
               <logger name="org.hibernate.validator" level="ERROR"/>
               <logger name="org.jboss.netty" level="ERROR"/>
               <logger name="org.apache.http" level="ERROR"/>
               <logger name="oshi.util" level="ERROR"/>
               <logger name="io.confluent.kafka.serializers" level="ERROR"/>
               <logger name="org.apache.catalina.loader" level="ERROR"/>
               <logger name="org.opengauss.core.v3" level="ERROR"/>
               <logger name="com.alibaba.druid.pool" level="ERROR"/>
           </Loggers>

       </Configuration>